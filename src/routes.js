import React from "react";
import { Redirect } from "react-router-dom";

// Layout Types
import { BaseLayout } from "./layouts";
// Route Views
import Dashboard from "./views/Dashboard/Dashboard";
import Buttons from "./views/Components/ButtonsContainer";
import Badge from "./views/Components/BadgeContainer";
import Card from "./views/Components/CardsContainer";
import Alert from "./views/Components/AlertContainer";
import ProgressBar from "./views/Components/ProgressBarContainer";
import Loader from "./views/Components/LoaderContainer";
import Nursing2 from "./views/Nursing2/Nursing2";
import Widgets from "./views/Widgets/WidgetsContainer";
import Forms from "./views/Forms/Forms";
import Tables from "./views/Tables/Tables";
import Pages from "./views/Pages/Pages";
import Charts from "./views/Charts/Charts";


var routes = [
  {
    path: "/med-x-force",
    exact: true,
    layout: BaseLayout,
    component: () => <Redirect to="/med-x-force/dashboard" />,
  },
  {
    path: "/med-x-force/dashboard",
    layout: BaseLayout,
    component: Dashboard,
  },

  {
    path: "/med-x-force/components/buttons",
    layout: BaseLayout,
    component: Buttons,
  },

  {
    path: "/med-x-force/components/badge",
    layout: BaseLayout,
    component: Badge,
  },

  {
    path: "/med-x-force/components/card",
    layout: BaseLayout,
    component: Card,
  },
  {
    path: "/med-x-force/components/alert",
    layout: BaseLayout,
    component: Alert,
  },
  {
    path: "/med-x-force/components/progressbar",
    layout: BaseLayout,
    component: ProgressBar,
  },
  {
    path: "/med-x-force/components/loader",
    layout: BaseLayout,
    component: Loader,
  },

  {
    path: "/med-x-force/nursing2",
    layout: BaseLayout,
    component: Nursing2,
  },
  {
    path: "/med-x-force/widgets",
    layout: BaseLayout,
    component: Widgets,
  },
  {
    path: "/med-x-force/forms",
    layout: BaseLayout,
    component: Forms,
  },
  {
    path: "/med-x-force/tables",
    layout: BaseLayout,
    component: Tables,
  },
  {
    path: "/med-x-force/pages",
    layout: BaseLayout,
    component: Pages,
  },

  {
    path: "/med-x-force/charts",
    layout: BaseLayout,
    component: Charts,
  },
];

export default routes;
