/*
 **Author: Santosh Kumar Dash
 **Author URL: http://santoshdash.epizy.com/
 **Github URL: https://github.com/quintuslabs/dashio-admin
 */

import React, { Component } from "react";
import "./StyleSheets/TransitionalCard.css";
import PropTypes from "prop-types";
import classNames from "classnames";
import { Button } from "../Button";
import ServiceNursing from "../../views/Nursing/ServiceNursing";

function TransitionalCard(props) {
  return (
    <div className={classNames("transitional--card")} style={{paddingBottom:10}}>
      <img src={props.image} />
      <div className="col-lg mb-12 col-sm-12 col-md-12" >
     <div className="card" >
     <div className="card-body">
       
        
        <div className="center__text" style={{fontSize:30}} >Service: Nursing</div>
        
            <br />
            
            
            <div className="item1">
                <div className="item-title" style={{float:"left"}}>
                Active
                </div>
                <i
                className={`fa fa-${props.icon1} ${props.color1} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count1}
              </i>
            </div>
            <br />
            <br />
            <hr />
            <div className="item2">
                <div className="item-title" style={{float:"left"}}>
                Inactive
                </div>
                <i
                className={`fa fa-${props.icon2} ${props.color2} fa-3x`}  style={{float:"right"}}
                aria-hidden="true"
              >
              {props.count2}
              </i>
            </div>
            <br />
            <br />
            <hr />
            
           
            
         
        </div>
      </div>
    </div>
    
      </div>
    
  );
}

TransitionalCard.propTypes = {
  title: PropTypes.string,
  body: PropTypes.string,
  buttonoTitle: PropTypes.string,
  image: PropTypes.any,
  className: PropTypes.string,
  onClick: PropTypes.func
};

// Specifies the default values for props:
TransitionalCard.defaultProps = {
  title: "Profile",
  image: "https://source.unsplash.com/collection/190727/900x900",
  body: "Click to see or edit your profile page.",
  buttonoTitle: "Readmore"
};

export default TransitionalCard;
