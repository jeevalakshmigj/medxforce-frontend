
import React, { Component } from "react";
import "./StyleSheets/Footer.css";
class Footer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <footer className="site-footer">
        <div className="footer-inner bg-white">
          <div className="row">
            <div className="col-sm-6">Copyright &copy; .............</div>
            <div className="col-sm-6 text-right">
              Designed by <a href="#">RQB</a>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
